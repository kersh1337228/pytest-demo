# pytest-demo &middot; [![GitHub license](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/kersh1337228/pytest-demo/-/blob/master/LICENSE)
Simple python calculator application unit testing with pytest.
