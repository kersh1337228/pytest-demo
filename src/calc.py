import math
from typing import Callable, Never


real = int | float


def binary_operation(
        operation: Callable[[real, real], real]
) -> Callable[[real, real], real]:
    def wrap(a: real, b: real) -> real | Never:
        errors = []
        if not isinstance(a, real):
            errors.append(TypeError(f'''First argument type mismatch: 
                expected type "real", got "{type(a).__name__}"'''))
        if not isinstance(b, real):
            errors.append(TypeError(f'''Second argument type mismatch: 
                expected type "real", got "{type(b).__name__}"'''))
        if errors:
            raise ExceptionGroup('Argument type mismatch', errors)

        return operation(a, b)
    return wrap


@binary_operation
def add(a: real, b: real) -> real:
    return a + b


@binary_operation
def sub(a: real, b: real) -> real:
    return a - b


@binary_operation
def mul(a: real, b: real) -> real:
    return a * b


@binary_operation
def div(a: real, b: real) -> real:
    return a / b


@binary_operation
def pow(a: real, b: real) -> real:
    return a ** b


def unary_operation(
        operation: Callable[[real], real]
) -> Callable[[real], real]:
    def wrap(a: real) -> real | Never:
        if not isinstance(a, real):
            raise TypeError(f'''Argument type mismatch:
                expected type "real", got "{type(a).__name__}"''')

        return operation(a)
    return wrap


@unary_operation
def sqrt(a: real) -> real:
    return math.sqrt(a)


@unary_operation
def fact(a: real) -> real:
    return math.gamma(a + 1)
