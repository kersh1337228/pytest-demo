import pytest
import math
from src import calc
from typing import Never


@pytest.fixture
def default_args() -> tuple[float, float]:
    return 3.14, 2.7


@pytest.fixture
def type_mismatch() -> tuple[str, bool]:
    return '3.14', True


def test_add(
        default_args: tuple[float, float],
        type_mismatch: tuple[str, bool]
) -> None | Never:
    with pytest.raises(ExceptionGroup) as err_info:
        calc.add(*type_mismatch)
    assert err_info.group_contains(TypeError), \
        'No TypeError raised due to type mismatch'

    assert (calc.add(*default_args) ==
            default_args[0] + default_args[1]), \
        'Wrong add operation result'


def test_sub(
        default_args: tuple[float, float],
        type_mismatch: tuple[str, bool]
) -> None | Never:
    with pytest.raises(ExceptionGroup) as err_info:
        calc.add(*type_mismatch)
    assert err_info.group_contains(TypeError), \
        'No TypeError raised due to type mismatch'

    assert (calc.sub(*default_args) ==
            default_args[0] - default_args[1]), \
        'Wrong subtract operation result'


def test_mul(
        default_args: tuple[float, float],
        type_mismatch: tuple[str, bool]
) -> None | Never:
    with pytest.raises(ExceptionGroup) as err_info:
        calc.add(*type_mismatch)
    assert err_info.group_contains(TypeError), \
        'No TypeError raised due to type mismatch'

    assert (calc.mul(*default_args) ==
            default_args[0] * default_args[1]), \
        'Wrong multiplication operation result'


def test_div(
        default_args: tuple[float, float],
        type_mismatch: tuple[str, bool]
) -> None | Never:
    with pytest.raises(ExceptionGroup) as err_info:
        calc.add(*type_mismatch)
    assert err_info.group_contains(TypeError), \
        'No TypeError raised due to type mismatch'

    with pytest.raises(ZeroDivisionError):
        calc.div(default_args[0], 0)

    assert (calc.div(*default_args) ==
            default_args[0] / default_args[1]), \
        'Wrong division operation result'


def test_pow(
        default_args: tuple[float, float],
        type_mismatch: tuple[str, bool]
) -> None | Never:
    with pytest.raises(ExceptionGroup) as err_info:
        calc.add(*type_mismatch)
    assert err_info.group_contains(TypeError), \
        'No TypeError raised due to type mismatch'

    assert (calc.pow(*default_args) ==
            default_args[0] ** default_args[1]), \
        'Wrong exponentiation operation result'


def test_sqrt(
        default_args: tuple[float, float],
        type_mismatch: tuple[str, bool]
) -> None | Never:
    with pytest.raises(TypeError):
        calc.sqrt(type_mismatch[0])

    with pytest.raises(ValueError):
        calc.sqrt(-default_args[0])

    assert (calc.sqrt(default_args[0]) ==
            math.sqrt(default_args[0])), \
        'Wrong square root operation result'


def test_fact(
        default_args: tuple[float, float],
        type_mismatch: tuple[str, bool]
) -> None | Never:
    with pytest.raises(TypeError):
        calc.fact(type_mismatch[0])

    assert (calc.fact(default_args[0]) ==
            math.gamma(default_args[0] + 1)), \
        'Wrong factorial operation result'
