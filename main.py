import sys
import pytest
from src import calc
from typing import Never


def main(*argv: str) -> int | Never:
    argc = len(argv)
    if argc == 1:
        print('No command specified', file=sys.stderr)
        return 1

    command = argv[1]
    if not command.startswith('--'):
        print('All commands must start with "--"', file=sys.stderr)
        return 1

    match command:
        case '--help':
            print(f'''usage: python main.py [command [arguments]]
    commands:
        --add  <a> <b>:  a + b
        --sub  <a> <b>:  a - b
        --mul  <a> <b>:  a * b
        --div  <a> <b>:  a / b
        --pow  <a> <b>:  a ^ b
        --sqrt <a>    :  sqrt(a)
        --fact <a>    :  a!
        --test        :  run complete calculator unit testing
        --help        :  show this help message
            ''')

        case '--add' | '--sub' | '--mul' | '--div' | '--pow':
            if argc != 4:
                print(
                    f'''Wrong number of operands for {command[2:]} operation:
                        expected 4, got {argc}''', file=sys.stderr)
                return 1
            a, b = map(float, argv[2:])
            match command:
                case '--add':
                    print(f'{a} + {b} = {calc.add(a, b)}')
                case '--sub':
                    print(f'{a} - {b} = {calc.sub(a, b)}')
                case '--mul':
                    print(f'{a} * {b} = {calc.mul(a, b)}')
                case '--div':
                    print(f'{a} / {b} = {calc.div(a, b)}')
                case '--pow':
                    print(f'{a} ^ {b} = {calc.pow(a, b)}')

        case '--sqrt' | '--fact':
            if argc != 3:
                print(
                    f'''Wrong number of operands for {command[2:]} operation:
                        expected 3, got {argc}''', file=sys.stderr)
                return 1
            a = float(argv[2])
            match command:
                case '--sqrt':
                    print(f'sqrt({a}) = {calc.sqrt(a)}')
                case '--fact':
                    print(f'{a}! = {calc.fact(a)}')

        case '--test':
            rv = pytest.main(['src/tests.py'])
            return rv

    return 0


if __name__ == '__main__':
    sys.exit(main(*sys.argv))
